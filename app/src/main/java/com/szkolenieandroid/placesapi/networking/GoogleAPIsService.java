package com.szkolenieandroid.placesapi.networking;

import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Query;

/**
 * Created by dmitry on 25/11/14.
 */
public interface GoogleAPIsService {

    @GET("/textsearch/json")
    void getPlacesForCity(@Query("query") String cityName, @Query("key") String key, Callback<PlacesList> callback);

//    https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=-33.8670522,151.1957362&radius=500&types=food&name=cruise&key=AddYourOwnKeyHere

    @GET("/nearbysearch/json")
    void getPlacesForTypeLocation(@Query("location") String location
            , @Query("radius") int radius
            , @Query("name") String name
            , @Query("key") String key
            , Callback<PlacesList> callback);

}
