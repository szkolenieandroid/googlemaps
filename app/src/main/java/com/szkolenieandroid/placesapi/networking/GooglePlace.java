package com.szkolenieandroid.placesapi.networking;

import com.google.android.gms.maps.model.LatLng;
import com.google.gson.annotations.SerializedName;

/**
 * Created by dmitry on 25/11/14.
 */
public class GooglePlace {

    @SerializedName("formatted_address")
    private String address;

    @SerializedName("geometry")
    private Geometry geometry;

    private String name;

    public String getAddress() {
        return address;
    }

    public String getName() {
        return name;
    }

    public LatLng getLocation() {
        return new LatLng(geometry.location.lat, geometry.location.lng);
    }

    class Geometry {
        GLocation location;
    }

    class GLocation {
        float lat;
        float lng;
    }
}

/*
"formatted_address" : "Okopowa 58-72, Warsaw, Poland",
         "geometry" : {
            "location" : {
               "lat" : 52.247046,
               "lng" : 20.979294
            }
         },
         "icon" : "http://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png",
         "id" : "b76d0967baa7b57b04732ad8bc722435167c071b",
         "name" : "Euronet"
 */