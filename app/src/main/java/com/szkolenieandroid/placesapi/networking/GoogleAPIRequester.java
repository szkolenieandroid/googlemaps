package com.szkolenieandroid.placesapi.networking;

import android.location.Location;

import retrofit.Callback;
import retrofit.RestAdapter;

/**
 * Created by dmitry on 25/11/14.
 */
public class GoogleAPIRequester {

    private static RestAdapter restAdapter;
    private static final String GOOGLE_API_ENDPOINT = "https://maps.googleapis.com/maps/api/place";
    private static final String GOOGLE_API_KEY = "AIzaSyCpolzbNq_XcAF78ofElBSU7zR-b0sYmfc";

    private RestAdapter getRestAdapter() {
        if(restAdapter == null) {
            restAdapter = new RestAdapter.Builder().setEndpoint(GOOGLE_API_ENDPOINT).build();
        }
        return restAdapter;
    }

    public void requestPlacesInCity(String placeName,  String cityName, Callback<PlacesList> callback) {

        RestAdapter restAdapter = getRestAdapter();
        GoogleAPIsService service = restAdapter.create(GoogleAPIsService.class);
        String placesInCity = placeName + "+in+" +cityName;
        service.getPlacesForCity(placesInCity, GOOGLE_API_KEY, callback);
    }

    public void getPlacesForTypeLocation(String placeName,  Location location, Callback<PlacesList> callback) {

        RestAdapter restAdapter = getRestAdapter();
        GoogleAPIsService service = restAdapter.create(GoogleAPIsService.class);
        String locationString = location.getLatitude() + "," +location.getLongitude();
        service.getPlacesForTypeLocation(locationString,15000,placeName, GOOGLE_API_KEY, callback);
    }
}
