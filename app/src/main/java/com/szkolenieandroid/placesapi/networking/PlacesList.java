package com.szkolenieandroid.placesapi.networking;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by dmitry on 25/11/14.
 */
public class PlacesList {

    @SerializedName("results")
    private List<GooglePlace> places;

    public List<GooglePlace> getPlaces() {
        return places;
    }
}
