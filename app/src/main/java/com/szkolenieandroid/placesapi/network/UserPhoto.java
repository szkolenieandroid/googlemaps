package com.szkolenieandroid.placesapi.network;

import com.google.gson.annotations.SerializedName;

/**
 * Created by dmitry on 20/11/14.
 */
public class UserPhoto {

    @SerializedName("images")
    private LocalImages images;


    public String getPhotoStandardURL() {
        return images.standardResolution.url;
    }
    public String getPhotoThumbnailURL() {
        return images.thumbnail.url;
    }
    class LocalImages {
        @SerializedName("standard_resolution")
        private LocalImage standardResolution;

        @SerializedName("thumbnail")
        private LocalImage thumbnail;
    }

    class LocalImage {

        private String url;
    }
}
