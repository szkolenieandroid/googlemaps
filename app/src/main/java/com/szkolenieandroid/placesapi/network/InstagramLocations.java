package com.szkolenieandroid.placesapi.network;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by dmitry on 28/11/14.
 */
public class InstagramLocations {

    @SerializedName("data")
    private List<InstagramLocation> locations;

    public List<InstagramLocation> getLocations() {
        return locations;
    }

    public class InstagramLocation{
        private long id;

        public long getId() {
            return id;
        }
    }
}
/*
{ "id" : "15673963",
        "latitude" : 52.249704999999999,
        "longitude" : 20.982679000000001,
        "name" : "Regionalna Instytucja Finansująca"
      },
 */