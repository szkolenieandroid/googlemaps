package com.szkolenieandroid.placesapi.network;

import android.location.Location;
import android.util.Log;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by dmitry on 28/11/14.
 */
public class InstagramAPIRequester {

    public void loadPhotosForLocation(Location location, final Callback<UserMedia> callback) {

        InstagramApiService service = getInstagramApiService();

        service.getLocations(location.getLatitude(), location.getLongitude(), "05040b60aca5430d819dde8bb92818db", new Callback<InstagramLocations>() {
            @Override
            public void success(InstagramLocations instagramLocations, Response response) {
                requestPhotosForLocation(instagramLocations.getLocations().get(0), callback);
            }

            @Override
            public void failure(RetrofitError error) {

            }
        });
    }

    private void requestPhotosForLocation(InstagramLocations.InstagramLocation instagramLocation, Callback<UserMedia> callback) {
        InstagramApiService service = getInstagramApiService();

        service.getLocationPhotos(instagramLocation.getId(), "05040b60aca5430d819dde8bb92818db", callback);
    }

    private InstagramApiService getInstagramApiService() {
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint("https://api.instagram.com/v1/")
                .build();

        return restAdapter.create(InstagramApiService.class);
    }

}
