package com.szkolenieandroid.placesapi;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.view.View;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.szkolenieandroid.placesapi.network.InstagramAPIRequester;
import com.szkolenieandroid.placesapi.network.UserMedia;
import com.szkolenieandroid.placesapi.network.UserPhoto;
import com.szkolenieandroid.placesapi.networking.GoogleAPIRequester;
import com.szkolenieandroid.placesapi.networking.GooglePlace;
import com.szkolenieandroid.placesapi.networking.PlacesList;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class PlacesActivity extends FragmentActivity {

    private GoogleMap mMap; // Might be null if Google Play services APK is not available.

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_places);
        setUpMapIfNeeded();
    }

    @Override
    protected void onResume() {
        super.onResume();
        setUpMapIfNeeded();
    }


    //https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=52.2493028%2C20.98430658&radius=15000&name=atm&key=AIzaSyCpolzbNq_XcAF78ofElBSU7zR-b0sYmfc
    private void requestPlaces(Location location) {

        GoogleAPIRequester requester = new GoogleAPIRequester();
        requester.getPlacesForTypeLocation("restaurant", location, new Callback<PlacesList>() {
            @Override
            public void success(PlacesList placesList, Response response) {
                presentPlaces(placesList);
            }

            @Override
            public void failure(RetrofitError error) {

            }
        });
    }

    private void presentPlaces(PlacesList placesList) {
        for (GooglePlace place : placesList.getPlaces()) {
            mMap.addMarker(new MarkerOptions().position(place.getLocation()).title(place.getName()).icon(
                    BitmapDescriptorFactory.fromResource(R.drawable.atm)));
        }

    }


    private void setUpMapIfNeeded() {
        if (mMap == null) {
            mMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map))
                    .getMap();
            if (mMap != null) {
                setUpMap();
                mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {

                    @Override
                    public void onMapClick(LatLng newLatLon) {
                        mMap.addMarker(new MarkerOptions().position(newLatLon).title("My Click"));

                        requestPlaces(newLatLon);
                    }
                });
                mMap.setMyLocationEnabled(true);
                mMap.getUiSettings().setZoomControlsEnabled(false);
                mMap.getUiSettings().setCompassEnabled(false);
                mMap.getUiSettings().setZoomGesturesEnabled(false);
            }
        }
    }

    private void requestPlaces(LatLng newLatLon) {
        Location location = new Location("");
        location.setLatitude(newLatLon.latitude);
        location.setLongitude(newLatLon.longitude);
        requestInstagramMedia(location);
    }

    private void requestInstagramMedia(Location location) {
        InstagramAPIRequester instagramAPIRequester = new InstagramAPIRequester();
        instagramAPIRequester.loadPhotosForLocation(location, new Callback<UserMedia>() {
            @Override
            public void success(UserMedia userMedia, Response response) {
                presentMarkersForMedia(userMedia);
            }

            @Override
            public void failure(RetrofitError error) {

            }
        });
    }

    private void presentMarkersForMedia(UserMedia userMedia) {
        for (UserPhoto userPhoto : userMedia.getUserPhotos()) {
            PresentImageOnMapTask presentImageOnMapTask = new PresentImageOnMapTask();
            presentImageOnMapTask.photoURL = userPhoto.getPhotoThumbnailURL();
            presentImageOnMapTask.execute();
        }
    }

    private void setUpMap() {
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(52.2493028, 20.98430658), 15));
    }

    public void setSatellite(View view) {
        mMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
    }

    public void setTerrain(View view) {
        mMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
    }

    public void setHybrid(View view) {
        mMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
    }

    public void setNormal(View view) {
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
    }


    class PresentImageOnMapTask extends AsyncTask<Void, Void, Void> {

        String photoURL;
        Bitmap bmImg;

        @Override
        protected Void doInBackground(Void... params) {
            URL url = null;
            try {
                url = new URL(photoURL);

                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setDoInput(true);
                conn.connect();
                InputStream is = conn.getInputStream();
                bmImg = BitmapFactory.decodeStream(is);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            mMap.addMarker(new MarkerOptions().position(new LatLng(52.2493028, 20.98430658)).title("My Click").icon(BitmapDescriptorFactory.fromBitmap(bmImg)));
        }
    }
}
